import discord
from discord.ext import commands, tasks
import random
import json
import os
from discord_slash import SlashCommand
from itertools import cycle
from PIL import Image
from io import BytesIO
from discord_components import Button, Select, SelectOption, ComponentsBot

bot = ComponentsBot('pcc!')


def get_prefix(cient, message):
    with open('prefixes.json', 'r') as f:
        prefixes = json.load(f)

    return prefixes[str(message.guild.id)]


intents = discord.Intents().all()
client = commands.Bot(command_prefix=get_prefix, intents=intents)
slash = SlashCommand(client, sync_commands=True)
client.remove_command("help")

status = cycle(['Op school', 'Huiswerk', 'Op Discord', 'pcc!help'])


@tasks.loop(seconds=300)
async def status_swap():
    await client.change_presence(activity=discord.Game(next(status)))


@client.event
async def on_ready():
    print('Bot is online')
    status_swap.start()


@client.event
async def on_guild_join(guild):
    with open('prefixes.json', 'r') as f:
        prefixes = json.load(f)

    prefixes[str(guild.id)] = '!'

    with open('prefixes.json', 'w') as f:
        json.dump(prefixes, f, indent=4)


@client.event
async def on_member_join(member):
    await client.get_channel(948550360669241405).send(
        f"{member.name} has joined the server!")
    await member.send(
        "Hoi! Welkom op de server van het PCC Lyceum!\n Deze server is bedoeld voor huiswerk hulp. Probeer echt alleen te helpen en niet het antwoord voor te zeggen. Als het antwoord word voor gezegt dan word dat bericht waarschijnlijk verwijderd.\n Mijn bot prefix is pcc!. Als je iets niet snapt over de bot kan je altijd pcc!help doen, of 1 van de Bot Developers te taggen. (Die hebben de rol Bot Developer.) Als je iets niet snapt over de server help pcc!serverinfo misschien, en anders kan je het gewoon aan een ander lid vragen.\n Veel plezier!"
    )


@slash.slash(description="Shows the bots latency")
async def ping(ctx):
    await ctx.send(f'Bot speed = {round(client.latency * 1000)}ms')


async def on_guild_remove(guild):
    with open('prefixes.json', 'r') as f:
        prefixes = json.load(f)

    prefixes.pop(str(guild.id))

    with open('prefixes.json', 'w') as f:
        json.dump(prefixes, f, indent=4)


@client.command(aliases=['speed'])
async def ping(ctx):
    await ctx.send('Pong!')


@client.command()
async def avatar(ctx, member: discord.Member = None):
    if member == None:
        member = ctx.author

    memberAvatar = member.avatar_url

    avaEmbed = discord.Embed(title=f"{member.name}'s Avatar")
    avaEmbed.set_image(url=memberAvatar)

    await ctx.send(embed=avaEmbed)


@client.command(aliases=['up', 'nieuw'])
async def update(ctx):
    updEmbed = discord.Embed(title="Update versie 1.1",
                             description="Voor elke update wat er nieuw is.",
                             color=discord.Color.blurple())
    updEmbed.set_thumbnail(
        url=
        "https://play-lh.googleusercontent.com/05TSk0wjFFgCM5teTeP9kaDvZNPVdweXaIl4RF4saV57ODwDU6Ulo6DY-s-R7-30-d8"
    )

    updEmbed.add_field(
        name="Bot Launch! 🚀",
        value=
        "De PCC Bot is oficieel online en de meeste functies werken!\n Voor meer info over andere fucnties, gebruik pcc!beta",
        inline=False)
    updEmbed.add_field(
        name="Slash commands!",
        value=
        "Er zijn nu ook slash commands! Met deze commands (Waarvan de meeste commands alleen door Moderators gebruikt kunnen worden.)\n Voor alle commands(Slash en andere) gebruik pcc!help",
        inline=False)
    updEmbed.add_field(
        name="Muziek!",
        value=
        "De muziek functie is up and running! Dit betekent dat de muziek functie altijd te vinden is en dat iedereen een liedje kan toevoegen aan de queue!",
        inline=False)

    await ctx.send(embed=updEmbed)


@client.command(aliases=['beta'])
async def béta(ctx):
    betaEmbed = discord.Embed(
        title="Béta functies",
        description="Alle béta functies en een beschijving",
        color=discord.Color.dark_red())

    betaEmbed.add_field(
        name="/clear",
        value=
        "/clear kan gebruikt worden als een snelle manier om berichten uit de chat te verwijderen.\n Het command werkt, maar de optie om te kiezen hoe veel berichten er verwijderd moeten worden werkt nog niet.",
        inline=False)
    betaEmbed.add_field(
        name="MUZIEK",
        value=
        "De muziek functie kan gebruikt worden in een spraak kanaal om (Bijvoorbeeld tijdens huiswerk maken) naar muziek te luisteren.\n Deze functie komt er zoizo.",
        inline=False)
    betaEmbed.add_field(
        name="Levels",
        value=
        "Er is al een levelings systeem via MEE6 maar misschien komt er in de toekomst een manier om (zoals in WRTS) punten te kunnen behalen als je mensen helpt met hun huiswerk.\n Of dit komt is nog niet 100% zeker.",
        inline=False)

    await ctx.send(embed=betaEmbed)


@client.command(aliases=['cmd', 'cmds', 'help'])
async def commands(ctx):
    cmdEmbed = discord.Embed(
        title="Alle commands en info",
        description=
        "Dit is voor de PCC Bot en voor de PCC Lyceum server.\n Voor meer informatie over de Béta features gebruik pcc!beta",
        color=discord.Color.dark_blue())

    cmdEmbed.add_field(
        name="Slash Commands",
        value=
        "``/ban``, ``/unban``, ``/kick``, ``/slowmode``, ``/mute``, ``/unmute``",
        inline=False),
    cmdEmbed.add_field(name="Features",
                       value="``eightball``, ``zeg``, ``avatar``",
                       inline=False)
    cmdEmbed.add_field(name="Server help",
                       value="``serverinfo``, ``help``",
                       inline=True)
    cmdEmbed.add_field(name="Serverprefix", value="`pcc!`", inline=True),
    cmdEmbed.add_field(name="Béta",
                       value="`/clear`, `MUZIEK`, `levels`",
                       inline=False)
    await ctx.send(embed=cmdEmbed)


@client.command(aliases=['8ball', '8b'])
async def eightball(ctx, *, question):
    responses = [
        "It is certain.", "It is decidedly so.", "Zonder te twijfelen",
        "Ja - Zeker weten", "You may rely on it.", "As I see it, yes.",
        "Most likely.", "Outlook good.", "Ja.", "Signs point to yes.",
        "Reply hazy, try again.", "Ask again later.",
        "Better not tell you now.", "Cannot predict now.",
        "Concentrate and ask again.", "Reken er niet op",
        "Mijn antwoord is nee.", "My sources say no.", "Outlook not so good.",
        "Very doubtful."
    ]
    await ctx.send(
        f':8ball: Vraag: {question}\n:8ball: Antwoord: {random.choice(responses)}'
    )


@client.command()
async def zeg(ctx, saymsg=None):
    if saymsg == None:
        return await ctx.send('Je moet wel vertellen wat ik moet zeggen!')
    sayEmbed = discord.Embed(title=f"{ctx.author} zegt",
                             description=f"{saymsg}")
    await ctx.send(embed=sayEmbed)


  
# Small-Talk

@client.command(aliases=["Hey", "hey", "hallo", "Hoi", "hoi"])
async def Hallo(ctx):
  hey = ["Hallo", "Hey, hoe gaat het?", "Sup?", "Hoi!"]
  await ctx.send(hey)

@client.command(aliases=["Hoe is 't?", "Hoest?", "Hoe gaat het?"])
async def Hoe_Gaat_Het(ctx):
  goed = ["Altijd goed!", "Prima.", "Nu goed", "Niet echt goed", "Goed!"]
  await channel.send(random.choice(goed))

# end Small-Talk


@client.command(aliases=['prefix'])
async def setprefix(ctx, prefixset):

    if (prefixset == None):
        prefixset = 'd!'

    with open('prefixes.json', 'r') as f:
        prefixes = json.load(f)

        prefixes[str(ctx.guild.id)] = prefixset

    with open('prefixes.json', 'w') as f:
        json.dump(prefixes, f, indent=4)

    await ctx.send('The bot prefix has been changed.')


@slash.slash(description="Banned een member van de server")
async def ban(ctx, member: discord.Member, *, reason=None):
    if (not ctx.author.guild_permissions.kick_members):
        await ctx.send('This command requires the ``Kick Members`` permission')
        return
    await member.kick(reason=reason)
    await ctx.send(f'{member.mention} has been kicked')


@slash.slash(description="Kicked iemand van de server")
async def kick(ctx, member: discord.Member, *, reason=None):
    if (not ctx.author.guild_permissions.ban_members):
        await ctx.send('This command requires the ``Ban Members`` permission')
        return
    await member.ban(reason=reason)
    await ctx.send(f'{member.mention} has been banned')


@slash.slash(description="Unbanned iemand")
async def unban(ctx, *, member):
    if (not ctx.author.guild_permissions.ban_members):
        await ctx.send('This command requires the ``Ban Members`` perimission')
        return
    banned_users = await ctx.guild.bans()
    member_name, member_discriminator = member.split('#')

    for ban_entry in banned_users:
        user = ban_entry.user

        if (user.name, user.discriminator) == (member_name,
                                               member_discriminator):
            await ctx.guild.unban(user)
            await ctx.send(f'Unbanned {user.mention}')
            return


@slash.slash(description="Stelt een slowmode in in het kanaal waar je in zit.")
async def slowmode(ctx, time: int):

    try:
        if time == 0:
            await ctx.send('Slowmode off')
            await ctx.channel.edit(slowmode_delay=0)
        elif time > 21600:
            await ctx.send('De slowmode kan niet hoger dan 6 minuten.')
            return
        else:
            await ctx.channel.edit(slowmode_delay=time)
            await ctx.send(f'Slowmode set to {time} seconds!')
    except Exception:
        await Print('Oops!')


@client.command()
async def serverinfo(ctx):
    role_count = len(ctx.guild.roles)
    list_bots = [bot.mention for bot in ctx.guild.members if bot.bot]

    serverinfoEmbed = discord.Embed(timestamp=ctx.message.created_at,
                                    color=ctx.author.color)
    serverinfoEmbed.add_field(name='Naam',
                              value=f"{ctx.guild.name}",
                              inline=False)
    serverinfoEmbed.add_field(name='Aantal leden',
                              value=ctx.guild.member_count,
                              inline=False)
    serverinfoEmbed.add_field(name='Verificatieniveau',
                              value=str(ctx.guild.verification_level),
                              inline=False)
    serverinfoEmbed.add_field(name='Hoogste rol',
                              value=ctx.guild.roles[-2],
                              inline=False)
    serverinfoEmbed.add_field(name='Aantal rollen',
                              value=str(role_count),
                              inline=False)
    serverinfoEmbed.add_field(name='Bots',
                              value=', '.join(list_bots),
                              inline=False)

    await ctx.send(embed=serverinfoEmbed)


@slash.slash(description='Verwijderd een aantal berichten')
async def clear(ctx, amount=11):
    if (not ctx.author.guild_permissions.manage_messages):
        await ctx.send('Hier voor moet je de Manage Messages permissie hebben.'
                       )
        return

    amount = amount + 1
    if amount > 101:
        await ctx.send('Cannot delete more then 100 messages')
    else:
        await ctx.channel.purge(limit=amount)
        await ctx.send('Cleared Messages')


@slash.slash(
    description=
    "Als iemand gemute word kan hij/zij geen berichten sturen of spreken in spraak kanalen."
)
async def mute(ctx, member: discord.Member, *, reason=None):
    if (not ctx.author.guild_permissions.manage_messages):
        await ctx.send(
            'This command requires the ``Manege Messages`` perimission')
        return
    guild = ctx.guild
    muteRole = discord.utils.get(guild.roles, name="Muted")

    if not muteRole:
        await ctx.send('No mute role has been found. Creating mute role...')

        muteRole = await guild.create_role(name="Muted")

        for channel in guild.channel:
            await channel.set_permissions(muteRole,
                                          speak=False,
                                          send_messages=False,
                                          read_message_history=True,
                                          read_messages=True)
    await member.add_roles(muteRole, reason=reason)
    await ctx.send('User has been muted')
    await member.send(
        f"You have been muted from **{guild.name}** | Reason: **{reason}**")


@slash.slash(description="Verijderd de mute rol van iemand.")
async def unmute(ctx, member: discord.Member, *, reason=None):
    if (not ctx.author.guild_permissions.manage_messages):
        await ctx.send(
            'This command requires the ``Manege Messages`` perimission')
        return
    guild = ctx.guild
    muteRole = discord.utils.get(guild.roles, name="Muted")

    if not muteRole:
        await ctx.send('No muted role has not been found.')
        return

    await member.remove_roles(muteRole, reason=reason)
    await ctx.send('User has been unmuted')
    await member.send(
        f"You have been unmuted from **{guild.name}** | Reason: **{reason}**")


client.run('OTQ4NTUzNTYwNjc1NzQ1ODAz.Yh9fWA.8jqxHLFjhR00OpW_HkCDKoSzVrY')
